from django.db.models.fields import FloatField
from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
from django.db.models import F, ExpressionWrapper


import urllib
import urllib.parse
import json
import random

from .models import Word, Leaderboard
from django.utils import timezone
import requests
from decimal import Decimal


WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'iD4PYpo8YaIvBr5DD7hmbmRK'
ACTION_WHAT_IS_THE_WEATHER = 'WHAT_IS_THE_WEATHER'
ACTION_QUIZ = 'QUIZ'
ACTION_SEARCH_WORD = 'SEARCH_WORD'

def leaderboard(request):
    players = Leaderboard.objects.all().order_by('-total')
    first = players.first().total
    if first == 0:
        first = False
    
    players_correct_max = Leaderboard.objects.all().order_by('-correct')
    correct_max = players_correct_max.first().correct

    winrate = Leaderboard.objects.annotate(
            winrate1=ExpressionWrapper(
                (F('correct') * Decimal('1.0') / F('total')),
                output_field=FloatField())
            ).order_by('-winrate1')
    

    max_wr = 0
    for i in Leaderboard.objects.all():
        if i.get_winrate() >= max_wr:
            max_wr = i.get_winrate()
    
    context = {
        'players': players[:5],
        'max': first/100,
        'correct_max': correct_max/100,
        'winrates': winrate[:5],
        'max_wr': max_wr/100,
        'images': ["https://i.ibb.co/3c1Vqgn/pic-1.png",
        "https://i.ibb.co/RDRKkTQ/pic-2.png",
        "https://i.ibb.co/4VMWJpJ/pic-3.png",
        "https://i.ibb.co/dBGQ1Zf/pic-4.png",
        "https://i.ibb.co/sRyPLZT/pic-5.png",
        ]
    }
    return render(request, 'leaderboard.html', context)


def searchhis(request):  # Search history
    words = Word.objects.all().order_by('-search_time')

    context = {
        'words': words
    }
    return render(request, 'searchhis.html', context)


def clear_search(request):
    Word.objects.all().delete()
    data = {
        'text': "Someone has just reset the search history :sob: "
    }
    post_message(WEBHOOK_URL, data)

    return redirect(searchhis)


def translate(sentence):

    url = "https://microsoft-translator-text.p.rapidapi.com/translate"

    querystring = {"api-version": "3.0", "to": "ja",
                   "textType": "plain", "profanityAction": "NoAction"}

    payload = "[\r{\r\"Text\": \"\"\r}\r]"
    payload = payload[:13]+sentence+payload[13:]
    headers = {
        'content-type': "application/json",
        'x-rapidapi-key': "1b936bf2camsh40cfd6027411617p191c4djsn6f1b7e2ac007",
        'x-rapidapi-host': "microsoft-translator-text.p.rapidapi.com"
    }

    response = requests.post(
        url, data=payload, headers=headers, params=querystring)
    return((response.json()[0]['translations'][0]['text']))


@csrf_exempt
def ebui(request):
    if request.method != 'POST':
        return JsonResponse({})

    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'text': '<@{}> {}'.format(user_id,  translate(content)),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

# Response when what the user initially put in cannot be used to fetch anything
# Return an input block so the user can write it in again
def input_query(result_text, action_id, label_text):
    result = {
        'blocks': [
            {
                "type": "section",
                "block_id": "section1",
                "text": {
                    "type": "mrkdwn",
                    "text": result_text
                },
                "accessory": {
                    "type": "image",
                    "image_url": "https://i.ibb.co/s58RZSj/Screen-Shot-2021-07-07-at-13-08-30.png",
                    "alt_text": "be' Bui"
                }
            },
            {
                'type': 'divider',
            },
            {
                "dispatch_action": True,
                "type": "input",
                "element": {
                    "type": "plain_text_input",
                    "action_id": action_id
                },
                "label": {
                    "type": "plain_text",
                    "text": label_text,
                    "emoji": True
                }
            }

        ],
        'response_type': 'in_channel'
    }
    return result

# Response when the given input cannot be used to fetch anything
def wrong_input(response_text):
    response = {
        'blocks': [
            {
                'type': 'divider',
            },
            {
                'type': 'section',

                'text': {
                    'type': 'mrkdwn',
                    'text': response_text,
                },
                "accessory": {
                    'type': 'image',
                    'image_url': 'https://i.ibb.co/mh41xDK/youmumad.png',
                    'alt_text': 'Youmu pouting, cannot find that city',
                },
            }
        ],
        'response_type': 'in_channel'
    }
    return response

# Return a response block if the user gives a place that can be searched using openweatherdata API
def weather_result(user_id, selected_town):
    try:
        selected_value = urllib.parse.quote(selected_town, safe="/:?+,")
        weather_api_url = "https://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid={}".format(
            selected_value, "737a23125cc4cba472434ed2baecdced")
        req = urllib.request.Request(weather_api_url)
        res = urllib.request.urlopen(req)
        content = json.loads(res.read().decode('utf-8'))
        res.close()
        emoji = ':face_with_monocle:'
        if 'clear' in content['weather'][0]['description']:
            emoji = ':sunny:'
        elif 'rain' in content['weather'][0]['description']:
            emoji = ':rain_cloud:'
        elif 'cloud' in content['weather'][0]['description']:
            emoji = ':cloud:'
        else:
            emoji = ':face_with_monocle:'

        response = {
            'text': '<@{}> The weather in {} ({}) now is {} {}\nTemperature: {}°C\nHumidity: {}% '.format(
                user_id, selected_town, content['sys']['country'], content['weather'][0]['description'], emoji,
                content['main']['temp'],
                content['main']['humidity']),
            'response_type': 'in_channel'
        }
    except:
        response = {
            'response_type': 'Meodatrang123'
        }
    return response
@csrf_exempt
def weather(request):
    if request.method != 'POST':
        return JsonResponse({})

    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']
    result = weather_result(user_id=user_id,selected_town=content)
    if result['response_type'] == 'Meodatrang123':
        result = input_query(
            result_text="Enter the name of the place you try to find the weather: ",
            action_id=ACTION_WHAT_IS_THE_WEATHER,
            label_text="Enter the city:")
    return JsonResponse(result)


@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})

    payload = json.loads(request.POST.get('payload'))
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    print(payload)  # please don't delete this

    user_id = payload['user']['id']
    response_url=payload['response_url']
    if payload['actions'][0]['action_id'] == ACTION_WHAT_IS_THE_WEATHER:
        selected_town = payload['actions'][0]['value']
        response = weather_result(user_id=user_id,selected_town=selected_town)
        if response['text'] == 'Meodatrang123':
            response = wrong_input(response_text = 
                "<@{}> Did you write a right name? :face_with_rolling_eyes:".format(user_id))
            
            
    elif payload['actions'][0]['action_id'] == ACTION_QUIZ:
        username = payload['user']['username']
        selected_value = str(payload['actions'][0]['selected_option']['value'])
        selected_option = payload['actions'][0]['selected_option']['text']['text']
        index, given_word, correct_option = selected_value.split(sep='=')
        try:
            answer = Leaderboard.objects.get(
                username=username, student_id=user_id)
        except Leaderboard.DoesNotExist:
            answer = Leaderboard.objects.create(
                username=username, student_id=user_id)
        answer.total = F('total') + 1

        if correct_option == selected_option:
            response_text = "Congratulations <@{}>! You have selected the right answer\n".format(
                user_id)
            answer.correct = F('correct') + 1
        else:
            response_text = "Sorry <@{}> :frowning:. You have selected the wrong answer this time.\n".format(
                user_id)


        answer.save()
        response_text += "The meaning for the word `{}` is Option {}: *{}*".format(
            given_word, int(index) + 1, correct_option)
        response = {
            'blocks': [
                {
                    'type': 'section',
                    'text': {
                        'type': 'mrkdwn',
                        'text': response_text
                    }
                }
            ]
        }
    elif payload['actions'][0]['action_id'] == ACTION_SEARCH_WORD:
        user_name = payload['user']['username']
        selected_value = payload['actions'][0]['value']
        response = search_result(user_id=user_id,user_name=user_name,content=selected_value)
        if response['response_type'] == 'Meodatrang123':
            response = wrong_input(
                response_text="<@{}> Did you write the word correctly? :face_with_rolling_eyes:".format(user_id))          
    else:
        raise SuspiciousOperation('Invalid request.')
    post_message(response_url, response)

    return JsonResponse({})

# get list of all words for quiz at a specific level


def get_words_for_quiz(level, words_count):
    page = random.randint(1, 10)
    jisho_api_url = "https://jisho.org/api/v1/search/words?keyword=jlpt-{}&page={}".format(
        level, page)
    req = urllib.request.Request(jisho_api_url)
    res = urllib.request.urlopen(req)
    # Choose k random unique elements from the list of data given
    data = random.sample(json.loads(
        res.read().decode('utf-8'))['data'], k=words_count)
    res.close()
    words = []
    for word in data:
        words.append({
            'word': word['slug'],
            'meaning': word['senses'][0]['english_definitions']
        })
    words_meaning = [str(words[i]['meaning'][0]) for i in range(words_count)]
    answer = {
        'word': words[0]['word'],
        'meaning': words_meaning[0]
    }
    random.shuffle(words_meaning)
    return (answer, words_meaning)


@csrf_exempt
def quiz(request):
    if request.method != 'POST':
        return JsonResponse({})

    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']
    # take only word_counts first words's meaning
    words_count = 4
    (answer, words_meaning) = get_words_for_quiz(content, words_count)
    result = {
        'blocks': [
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '<@{}> This is a small quiz for your understanding!\
                    \n What is the meaning of this kanji: *{}*?'
                    .format(user_id, answer['word'])
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'The meaning of this kanji is:',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': words_meaning[0],
                                'emoji': True
                            },
                            'value': str('{}={}={}'.format(0, answer['word'], answer['meaning']))
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': words_meaning[1],
                                'emoji': True
                            },
                            'value': str('{}={}={}'.format(1, answer['word'], answer['meaning']))
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': words_meaning[2],
                                'emoji': True
                            },
                            'value': str('{}={}={}'.format(2, answer['word'], answer['meaning']))
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': words_meaning[3],
                                'emoji': True
                            },
                            'value': str('{}={}={}'.format(3, answer['word'], answer['meaning']))
                        },
                    ],
                    'action_id': ACTION_QUIZ
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

# use for search in bot

# Return an image corresponding to the query
def get_image_url(query):
    try:

        client_id = 'FNFLUjSRYvFeNCaOPnNw32AIGei_kGwHzLUzH2xzAJ0'
        image_query = urllib.parse.quote(query, safe='/:?&+,')
        image_api_url = 'https://api.unsplash.com/search/photos?query=' + \
            image_query+'&client_id='+client_id
        req = urllib.request.Request(image_api_url)
        res = urllib.request.urlopen(req)
        content = json.loads(res.read().decode('utf-8'))
        res.close()
        return content['results'][0]['urls']['small']
    except:
        return 'https://bitsofco.de/content/images/2018/12/broken-1.png'

# Return the word and its corresponding JLPT Level, its meanings, how it's read and an example picture
def get_word(keyword):

    jisho_api_url = "https://jisho.org/api/v1/search/words?keyword={}".format(
        urllib.parse.quote(keyword))
    req = urllib.request.Request(jisho_api_url)
    res = urllib.request.urlopen(req)
    data = json.loads(res.read().decode('utf-8'))['data']
    res.close()
    # take the first word.
    word = {
        'word': data[0]['slug'],
        'level': 'unknown',
        'meaning': data[0]['senses'][0]['english_definitions'],
        'reading': data[0]['japanese'][0]['reading']
    }
    # check for katakana content
    # if yes, set it to reading.
    for c in word['word']:
        if c.isdigit():
            word['word'] = word['reading']
            break
    if len(data[0]['jlpt']):
        word['level'] = data[0]['jlpt'][0]
    meaning = ','.join(word['meaning'])
    image_url = get_image_url(word['meaning'][0])
    return (word['word'], word['level'].upper(), meaning, word['reading'], image_url)

# Return result block if user enter a word that can be searched using jisho API
def search_result(user_id, user_name, content):
    try:
        (word, level, meaning, reading, image_url) = get_word(keyword=content)
        save_word = Word(kanji=word, search_by=user_name, search_by_id=user_id, search_time=timezone.now(
        ), english_meaning=meaning, reading=reading, level=level, image_url=image_url)
        save_word.save()
        result = {
            "blocks": [
                {
                    "type": "section",
                    "text": {
                            "type": "mrkdwn",
                            "text": "Is this what you want <@{}>?".format(user_id)
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Word*: {}\n*Level*: {}\n*Meaning*: {}\n*Reading*: {}\n".format(word, level, meaning, reading)
                    },
                    "accessory": {
                        "type": "image",
                        "image_url": image_url,
                        "alt_text": "be' Bui"
                    }
                },
                {
                    "type": "divider"
                }
            ],
            'response_type': 'in_channel'
        }
        
    except:
        result = {
            'response_type': 'Meodatrang123'
        }
    return result


@csrf_exempt
def search(request):
    if request.method != 'POST':
        return JsonResponse({})

    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    # get user information
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']
    result = search_result(user_id=user_id,user_name=user_name, content=content)
    if result['response_type'] == 'Meodatrang123':
        result = input_query(
            result_text="Enter the word you want to search:",
            action_id=ACTION_SEARCH_WORD,
            label_text="Word:"
        )
    
    return JsonResponse(result)

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()
