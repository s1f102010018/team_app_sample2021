"""team_app_sample URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from team import views as team_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', team_views.searchhis, name='search_history'),
    path('clearsearch', team_views.clear_search, name='clear_search'),
    path('leaderboard', team_views.leaderboard, name='leaderboard'),
    path('api/ebui', team_views.ebui, name='api_ebui'),
    path('api/reply', team_views.reply, name='api_reply'), #weather
    path('api/weather', team_views.weather, name='api_weather'),
    path('api/search_word', team_views.search, name='api_search_word'),
    path('api/quiz', team_views.quiz, name='api_quiz'),
]
